-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: panini
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classe`
--

DROP TABLE IF EXISTS `classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classe` (
  `idClasse` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idClasse`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe`
--

LOCK TABLES `classe` WRITE;
/*!40000 ALTER TABLE `classe` DISABLE KEYS */;
INSERT INTO `classe` VALUES (1,'alessiotom@live.it','bo'),(2,'prova','prova@example.com'),(3,'ciao@bo.com','ciao'),(4,'555@555','555'),(5,'lollo.bragamc@gmail.com','202cb962ac59075b964b07152d234b70'),(6,'alessiotom@live.it','429c69df8c61a391519e9e37cf84b738'),(7,'alessiotom@live.it','429c69df8c61a391519e9e37cf84b738'),(8,'alessiotom@live.it','ad7532d5b3860a408fbe01f9455dca36'),(9,'alessiotom@live.it','ad7532d5b3860a408fbe01f9455dca36'),(10,'alessiotom@live.it','ad7532d5b3860a408fbe01f9455dca36'),(11,'likeangela@live.it','81dc9bdb52d04dc20036dbd8313ed055');
/*!40000 ALTER TABLE `classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contenuto`
--

DROP TABLE IF EXISTS `contenuto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenuto` (
  `FkProdotto` int(11) NOT NULL,
  `FkOrdine` int(11) NOT NULL,
  `quantita` int(11) DEFAULT NULL,
  PRIMARY KEY (`FkProdotto`,`FkOrdine`),
  KEY `contenutoOrdine_idx` (`FkOrdine`),
  CONSTRAINT `contenutoOrdine` FOREIGN KEY (`FkOrdine`) REFERENCES `ordine` (`idOrdine`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `contenutoProdotto` FOREIGN KEY (`FkProdotto`) REFERENCES `prodotto` (`idProdotto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenuto`
--

LOCK TABLES `contenuto` WRITE;
/*!40000 ALTER TABLE `contenuto` DISABLE KEYS */;
INSERT INTO `contenuto` VALUES (50,1,1),(57,1,1),(65,1,1),(93,2,1),(99,9,1),(103,21,1),(104,22,1),(105,23,1),(106,24,1),(107,25,1),(108,26,1),(109,27,1),(110,28,1),(111,29,1),(112,30,1),(113,31,1),(114,32,1),(115,33,1),(116,34,1),(117,35,1),(118,36,1),(119,37,1);
/*!40000 ALTER TABLE `contenuto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine`
--

DROP TABLE IF EXISTS `ordine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine` (
  `idOrdine` int(11) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `FkIdClasse` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOrdine`),
  KEY `fkClasse_idx` (`FkIdClasse`),
  CONSTRAINT `fkClasse` FOREIGN KEY (`FkIdClasse`) REFERENCES `classe` (`idClasse`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine`
--

LOCK TABLES `ordine` WRITE;
/*!40000 ALTER TABLE `ordine` DISABLE KEYS */;
INSERT INTO `ordine` VALUES (1,'0000-00-00',1),(2,'2019-02-12',2),(3,'2019-02-12',3),(4,'0000-00-00',1),(5,'0000-00-00',1),(6,'0000-00-00',1),(7,'0000-00-00',1),(8,'0000-00-00',1),(9,'0000-00-00',1),(18,'2019-04-04',1),(19,'2019-04-04',1),(20,'2019-04-04',1),(21,'2019-04-04',1),(22,'2019-04-04',3),(23,'2019-04-04',3),(24,'2019-04-04',3),(25,'2019-04-04',3),(26,'2019-04-04',3),(27,'2019-04-04',3),(28,'2019-04-04',3),(29,'2019-04-04',3),(30,'2019-04-04',3),(31,'2019-04-04',3),(32,'2019-04-05',3),(33,'2019-04-05',3),(34,'2019-04-05',3),(35,'2019-04-05',3),(36,'2019-04-05',3),(37,'2019-04-05',1);
/*!40000 ALTER TABLE `ordine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodotto`
--

DROP TABLE IF EXISTS `prodotto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodotto` (
  `idProdotto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `prezzo` varchar(50) NOT NULL,
  PRIMARY KEY (`idProdotto`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodotto`
--

LOCK TABLES `prodotto` WRITE;
/*!40000 ALTER TABLE `prodotto` DISABLE KEYS */;
INSERT INTO `prodotto` VALUES (50,'Ciabatta Pancetta','1.10â‚¬'),(51,'Ciabatta Pancetta','1.10â‚¬'),(52,'Ciabatta Pancetta','1.10â‚¬'),(53,'Ciabatta Pancetta','1.10â‚¬'),(54,'Ciabatta Pancetta','1.10â‚¬'),(55,'Ciabatta Pancetta','1.10â‚¬'),(56,'Ciabatta Pancetta','1.10â‚¬'),(57,'Pizza','1.10â‚¬'),(65,'Focaccia Liscia','1.00â‚¬'),(66,'Foc. Cotto Font','1.50â‚¬'),(67,'Focaccia Liscia','1.00â‚¬'),(68,'Ciabatta Cotto','1.10â‚¬'),(69,'Ciabatta Cotto','1.10â‚¬'),(70,'Ciabatta Salame','1.10â‚¬'),(71,'Ciabatta Salame','1.10â‚¬'),(72,'Ciabatta Cotto','1.10â‚¬'),(73,'Ciabatta Salame','1.10â‚¬'),(74,'Ciabatta Salame','1.10â‚¬'),(75,'Ciabatta Salame','1.10â‚¬'),(76,'Ciabatta Salame','1.10â‚¬'),(77,'Ciabatta Salame','1.10â‚¬'),(78,'Ciabatta Salame','1.10â‚¬'),(79,'Ciabatta Salame','1.10â‚¬'),(80,'Ciabatta Cotto','1.10â‚¬'),(81,'Pizza Wustel','1.10â‚¬'),(82,'Foc. Cotto Font','1.50â‚¬'),(83,'Ciabatta Salame','1.10â‚¬'),(84,'Ciabatta Salame','1.10â‚¬'),(85,'Focaccia Liscia','1.00â‚¬'),(86,'Focaccia Liscia','1.00â‚¬'),(87,'Ciabatta Pancetta','1.10â‚¬'),(88,'Ciabatta Salame','1.10â‚¬'),(89,'Pizza','1.10â‚¬'),(90,'Pizza Wustel','1.10â‚¬'),(91,'Focaccia Liscia','1.00â‚¬'),(92,'Foc. Salame Font','1.50â‚¬'),(93,'Pizza Wustel','1.10â‚¬'),(94,'We','2.00â‚¬'),(95,'We','2.00â‚¬'),(96,'We','2.00â‚¬'),(97,'We','2.00â‚¬'),(98,'We','2.00â‚¬'),(99,'We','2.00â‚¬'),(100,'We','2.00â‚¬'),(101,'We','2.00â‚¬'),(102,'Piada Tonn','2.00â‚¬'),(103,'Piada Tonn','2.00â‚¬'),(104,'Ciabatta Pancetta','1.10â‚¬'),(105,'Focaccia Liscia','1.00â‚¬'),(106,'Foc. Cotto Font','1.50â‚¬'),(107,'Cotoletta','1.10â‚¬'),(108,'Cotoletta','1.10â‚¬'),(109,'Ciabatta Salame','1.10â‚¬'),(110,'Ciabatta Pancetta','1.10â‚¬'),(111,'Pizza Wustel','1.10â‚¬'),(112,'Ciabatta Salame','1.10â‚¬'),(113,'Ciabatta Pancetta','1.10â‚¬'),(114,'Cotoletta Maio','1.10â‚¬'),(115,'Cotoletta Crudo','1.10â‚¬'),(116,'Pizza Wustel','1.10â‚¬'),(117,'Pizza','1.10â‚¬'),(118,'Speck Brie','1.10â‚¬'),(119,'Ciabatta Cotto','1.10â‚¬');
/*!40000 ALTER TABLE `prodotto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-10 19:52:42
