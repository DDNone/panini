<?php
  include "connection.php";
  //devo fare controllo che solo amministratore puo registrare nuovi utenti



  if(!empty($_SESSION["email"])){ //utente loggato
    //se loggato non deve registrarsi

  }else{
    echo '
    <section id="register" class="bg-primary text-white mb-0">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">REGISTRATI QUI!</h2>
        <hr class="star-light mb-5">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <form  action="index.php" method="post">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Email Address</label>
                    <input class="form-control" name="email"  type="email" placeholder="Email Address" required="required" >
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Insert Password </label>
                  <input class="form-control" name="pw" type="Password" placeholder="Password" required="required">
                  <p class="help-block text-danger"></p>
                </div>
              </div>

              <br>
              <div id="success"></div>
              <div class="form-group">
                <button type="submit" class="btn btn-xl btn-outline-light" >Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    ';
  }
 ?>
