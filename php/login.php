<?php
  include "connection.php";
  //devo fare controllo che solo amministratore puo registrare nuovi utenti



  if(!empty($_SESSION["email"])){ //utente loggato
    //se loggato non deve registrarsi
    ;

  }else{
    echo '
    <section id="register" >
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">EFFETTUA LOGIN!</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <form  action="index.php" method="post">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Email Address</label>
                    <input class="form-control" name="emailLogin"  type="email" placeholder="Email Address" required="required" >
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Insert Password </label>
                  <input class="form-control" name="pwLogin" type="Password" placeholder="Password" required="required">
                  <p class="help-block text-danger"></p>
                </div>
              </div>

              <br>
              <div id="success"></div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-xl" >Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    ';
  }
 ?>
